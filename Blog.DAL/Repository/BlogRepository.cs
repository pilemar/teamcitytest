﻿using System.Collections.Generic;
using Blog.DAL.Infrastructure;
using Blog.DAL.Model;
using System;

namespace Blog.DAL.Repository
{
    public class BlogRepository
    {
        private readonly BlogContext _context;

        public BlogRepository()
        {
            _context = new BlogContext();
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return _context.Posts;
        }

        public void AddPost(Post post) {

            if (post == null)
            {
                throw new ArgumentNullException("Post post is null");
            }

            if (post.Author.Length == 0 || post.Content.Length == 0)
            {
                throw new FormatException("Fields Author and Content are required!");
            }

            _context.Posts.Add(post);
            _context.SaveChanges();
        }
    }
}
